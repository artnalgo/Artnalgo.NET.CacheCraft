﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ServiceCollectionExtensions.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Artnalgo.NET.CacheCraft
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddArtnalgoMemoryCache(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddSingleton<ICacheService, MemoryCacheService>();
            return services;
        }

        public static IServiceCollection AddArtnalgoFileCache(this IServiceCollection services, string? cachePath = null)
        {
            services.AddSingleton<ICacheService>(provider => new FileCacheService(cachePath));
            return services;
        }

        // TODO: AddArtnalgoDistributedCache method for Redis or other distributed caches
        public static IServiceCollection AddArtnalgoDistributedCache(this IServiceCollection services, Action<DistributedCacheSettings> setupAction)
        {
            var settings = new DistributedCacheSettings();
            setupAction(settings);

         
            // implement the DistributedCacheService class and the DistributedCacheSettings class

            return services;
        }
    }

  
}


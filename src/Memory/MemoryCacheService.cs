﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: MemoryCacheService.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//
using System;
using Microsoft.Extensions.Caching.Memory;

namespace Artnalgo.NET.CacheCraft
{
    public class MemoryCacheService : ICacheService
    {
   

        Task<string?> ICacheService.GetKeyAsync(string name, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task<string?> ICacheService.GetNameAsync(string hash, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task<bool> ICacheService.KeyExistsAsync(string hash, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task<bool> ICacheService.NameExistsAsync(string name, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task ICacheService.RemoveByKeyAsync(string hash, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task ICacheService.RemoveByNameAsync(string name, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task<byte[]?> ICacheService.RetrieveByKeyAsync(string hash, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task<byte[]?> ICacheService.RetrieveByNameAsync(string name, string fileExtension)
        {
            throw new NotImplementedException();
        }

        Task ICacheService.StoreAsync(string filenameHash, string sourceHash, byte[] data, string fileExtension)
        {
            throw new NotImplementedException();
        }
    }
}


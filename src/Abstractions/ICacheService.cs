﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: ICacheService.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//

using System;
namespace Artnalgo.NET.CacheCraft
{
    public interface ICacheService
    {
        Task<bool> KeyExistsAsync(string hash, string fileExtension);
        Task<bool> NameExistsAsync(string name, string fileExtension);

        Task<byte[]?> RetrieveByKeyAsync(string hash, string fileExtension);
        Task<byte[]?> RetrieveByNameAsync(string name, string fileExtension);

        Task StoreAsync(string filenameHash, string sourceHash, byte[] data, string fileExtension);

        Task RemoveByKeyAsync(string hash, string fileExtension);
        Task RemoveByNameAsync(string name, string fileExtension);

        Task<string?> GetNameAsync(string hash, string fileExtension);
        Task<string?> GetKeyAsync(string name, string fileExtension);

    }

}


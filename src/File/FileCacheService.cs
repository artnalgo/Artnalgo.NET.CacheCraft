﻿// -----------------------------------------------------------------------------------------------------------------
// Copyright (c) Copyright (c) 2024 Artnalgo. All rights reserved.
// File: FileCacheService.cs
// Author: ozmpai
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// -----------------------------------------------------------------------------------------------------------------
//


namespace Artnalgo.NET.CacheCraft
{


    public class FileCacheService : ICacheService
    {
        private string _baseDir;

        public FileCacheService(string? baseDirname = null)
        {
            _baseDir = baseDirname == null ? Path.Combine(Path.GetTempPath(), "ArtnalgoCacheCraft") : baseDirname;
            Directory.CreateDirectory(_baseDir);
        }

        public Task<bool> KeyExistsAsync(string hash, string fileExtension)
        {
            var filePath = FindFilePathByKeyAndExtension(hash, fileExtension);
            return Task.FromResult(filePath != null);
        }

        public Task<bool> NameExistsAsync(string name, string fileExtension)
        {
            var filePath = FindFilePathByNameAndExtension(name, fileExtension);
            return Task.FromResult(filePath != null);
        }

        public async Task<byte[]?> RetrieveByKeyAsync(string hash, string fileExtension)
        {
            var filePath = FindFilePathByKeyAndExtension(hash, fileExtension);
            if (filePath != null)
            {
                return await File.ReadAllBytesAsync(filePath);
            }
            return null;
        }

        public async Task<byte[]?> RetrieveByNameAsync(string name, string fileExtension)
        {
            var filePath = FindFilePathByNameAndExtension(name, fileExtension);
            if (filePath != null)
            {
                return await File.ReadAllBytesAsync(filePath);
            }
            return null;
        }

        public async Task StoreAsync(string filenameHash, string sourceHash, byte[] data, string fileExtension)
        {
            var fileName = $"{filenameHash}_{sourceHash}{fileExtension}";
            var filePath = Path.Combine(_baseDir, fileName);
            await File.WriteAllBytesAsync(filePath, data);
        }

        public Task RemoveByKeyAsync(string hash, string fileExtension)
        {
            var filePath = FindFilePathByKeyAndExtension(hash, fileExtension);
            if (filePath != null)
            {
                File.Delete(filePath);
            }
            return Task.CompletedTask;
        }

        public Task RemoveByNameAsync(string name, string fileExtension)
        {
            var filePath = FindFilePathByNameAndExtension(name, fileExtension);
            if (filePath != null)
            {
                File.Delete(filePath);
            }
            return Task.CompletedTask;
        }

        public Task<string?> GetNameAsync(string hash, string fileExtension)
        {
            var filePath = FindFilePathByKeyAndExtension(hash, fileExtension);
            if (filePath != null)
            {
                var fileName = Path.GetFileName(filePath);
                var parts = fileName.Split('_', 2);
                if (parts.Length > 1) return Task.FromResult(parts[1].Split('.')[0] ?? null);
            }
            return Task.FromResult<string?>(null);
        }

        public Task<string?> GetKeyAsync(string name, string fileExtension)
        {
            var filePath = FindFilePathByNameAndExtension(name, fileExtension);
            if (filePath != null)
            {
                var fileName = Path.GetFileName(filePath);
                return Task.FromResult(fileName.Split('_')[0] ?? null);
            }
            return Task.FromResult<string?>(null);
        }

        private string? FindFilePathByKeyAndExtension(string hash, string fileExtension)
        {
            var files = Directory.GetFiles(_baseDir, $"{hash}_*{fileExtension}");
            return files.Length > 0 ? files[0] : null;
        }

        private string? FindFilePathByNameAndExtension(string name, string fileExtension)
        {
            var files = Directory.GetFiles(_baseDir, $"*_{name}{fileExtension}");
            return files.Length > 0 ? files[0] : null;
        }

    }


}